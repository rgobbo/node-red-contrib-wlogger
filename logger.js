const fs = require("fs");
const path = require("path");

module.exports = function (RED) {
  function loggerJs(config) {
    RED.nodes.createNode(this, config)
    this.loglevel = config.loglevel
    this.payload = config.payload;
    this.payloadType = config.payloadType;
    this.consoleLog = config.console;
    this.debugLog = config.debug;
    this.file = config.file;
    this.tracking = config.tracking;
    var node = this


    // ensure log directory exists
    var logDirectory = path.join(__dirname, 'logs');
    fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)

    const winston = require('winston');
    require('winston-daily-rotate-file');
    const MESSAGE = Symbol.for('message');
    const { format } = require('winston');

    const jsonFormatter = (logEntry) => {
      const base = { timestamp: new Date() };
      const json = Object.assign(base, logEntry)
      logEntry[MESSAGE] = JSON.stringify(json);
      return logEntry;
    }

    var transport = new (winston.transports.DailyRotateFile)({
      filename: 'logs/application-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '180d'
    });

    var logger = winston.createLogger({
      format: winston.format(jsonFormatter)(),
      transports: [
        transport
      ]
    });



    node.on('input', function (msg) {
      // var globalContext = this.context().global;
      // var logger = globalContext.get("logger");

      var tracking = '';
      if (this.tracking !== '') {
        tracking = ' [' + this.tracking + '] ';
      }
      var logContent = RED.util.evaluateNodeProperty(this.payload, this.payloadType, this, msg);
      var logText = '';
      if (typeof logContent === 'object' && logContent !== null) {
        logText = JSON.stringify(logContent);
        logMessage = this.tracking
        logContent['tracking'] = this.tracking;
        logContent['nodeId'] = this.id;
      } else {
        logText = logContent;
        logMessage = logContent;
        logContent = { nodeId: this.id, tracking: this.tracking };
      }


      if (this.debugLog) {
        switch (node.loglevel) {
          case 'INFO':
            logger.info(logMessage, logContent);
            break;
          case 'ERROR':
            logger.error(logMessage, logContent);
            break;
          case 'WARN':
            logger.warn(logMessage, logContent);
            break;
          case 'DEBUG':
            logger.debug(logMessage, logContent);
            break;
          case 'TRACE':
            logger.trace(logMessage, logContent);
            break;
          default:
            logger.info(logMessage, logContent);
        }
      }
      if (this.consoleLog) {
        var d = new Date();
        const month = d.toLocaleDateString('en-US', { month: 'short' });
        const day = ('00' + d.getDate()).slice(-2);;
        const hour = ('00' + d.getHours()).slice(-2);
        const minute = ('00' + d.getMinutes()).slice(-2);
        const second = ('00' + d.getSeconds()).slice(-2);

        var str = day + " " + month + " " +
          hour + ":" + minute + ":" + second;

        var logMessage = ' node-id:' + this.id + ' - ' + tracking + logText;

        console.log(str + ' - [' + node.loglevel + '] ' + logMessage);
      }

    })
  }
  RED.nodes.registerType("logger", loggerJs)
}